"use strict";
!function (t) {
    jQuery.fn.visualizeControls = function (i) {
        if (t(this)[0] && t(this)[0].type) {
            var n = t(this)[0].type;i = t.extend({ container: '<div class="' + n + '-container"></div>', inputWrap: '<div class="' + n + '-input custom-input"></div>', control: '<div class="' + n + '-control"></div>' }, i);var r = function r() {
                t(this).wrap(i.inputWrap).after(i.control), t(this).parents("label").length > 0 ? t(this).parents("label").wrap(i.container) : t(this).parent(t(i.inputWrap)).wrap(i.container);
            };return this.each(r);
        }
    };
}(jQuery), $(document).ready(function () {
    $('input[type="radio"]').visualizeControls(), $('input[type="checkbox"]').visualizeControls();
}), function (t) {
    jQuery.fn.visualizeControls = function (i) {
        if (t(this)[0] && t(this)[0].type) {
            var n = t(this)[0].type;i = t.extend({ container: '<div class="' + n + '-container"></div>', inputWrap: '<div class="' + n + '-input custom-input"></div>', control: '<div class="' + n + '-control"></div>' }, i);var r = function r() {
                t(this).wrap(i.inputWrap).after(i.control), t(this).parents("label").length > 0 ? t(this).parents("label").wrap(i.container) : t(this).parent(t(i.inputWrap)).wrap(i.container);
            };return this.each(r);
        }
    };
}(jQuery), $(document).ready(function () {
    $('input[type="radio"]').visualizeControls(), $('input[type="checkbox"]').visualizeControls();
});
(function ($) {
    jQuery.fn.visualizeControls = function (options) {
        if ($(this)[0] && $(this)[0].type) {
            var inputType = $(this)[0].type;

            options = $.extend({
                container: '<div class="' + inputType + '-container"></div>',
                inputWrap: '<div class="' + inputType + '-input custom-input"></div>',
                control: '<div class="' + inputType + '-control"></div>'
            }, options);

            var build = function build() {

                $(this).wrap(options.inputWrap).after(options.control);

                if ($(this).parents('label').length > 0) {
                    $(this).parents('label').wrap(options.container);
                } else {
                    $(this).parent($(options.inputWrap)).wrap(options.container);
                }
            };

            return this.each(build);
        }
    };
})(jQuery);

$(document).ready(function () {
    $('input[type="radio"]').visualizeControls();
    $('input[type="checkbox"]').visualizeControls();
});