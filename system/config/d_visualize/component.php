<?php
$_['d_visualize_component'] = array(
    'd_account_field_affiliate' => array(
        'template' => 'default',
    ),
    'd_address_field_affiliate' => array(
        'template' => 'default',
    ),
    'd_breadcrumb' => array(
        'template' => 'default',
    ),
    'd_button' => array(
        'template' => 'default',
    ),
    'd_button_continue' => array(
        'template' => 'default',
    ),
    'd_button_submit' => array(
        'template' => 'default',
    ),
    'd_cart' => array(
	    'template' => 'default',
    ),
    'd_search' => array(
	    'template' => 'default',
    ),
    'd_custom_field' => array(
        'template' => 'default',
    ),
    'd_name_field' => array(
        'template' => 'default',
    ),
    'd_notification' => array(
        'template' => 'default',
    ),
    'd_product_sort' => array(
        'template' => 'default',
    ),
    'd_product_list' => array(
    	'template' => 'default',
    ),
    'd_product_thumb' => array(
        'template' => 'default',
    ),
    'd_rating' => array(
        'template' => 'default',
    ),
    'd_layout' => array(
        'template' => 'default',
    ),
);

if (VERSION > '3.0.0.0') {
    $_['d_visualize_component']['d_account_field']['template'] = 'default';
    $_['d_visualize_component']['d_address_field']['template'] = 'default';
}